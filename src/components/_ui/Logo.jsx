import React from "react";
import logoImage from "images/umriyaafini.png"

const Logo = () => (
    <img height="50" src={logoImage} alt="Home" />
);

export default Logo;
